import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HomeScreen } from './src/home_screen';
import { MyBag } from './src/my_bag';
import { PokemonDetail } from './src/pokemon_detail';
const Stack = createNativeStackNavigator();
import { Provider } from 'react-redux'
import store from './src/redux/store';

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home"
          screenOptions={{ headerShown: false }}
        >
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="PokemonDetail" component={PokemonDetail} />
          <Stack.Screen name="MyBag" component={MyBag} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
