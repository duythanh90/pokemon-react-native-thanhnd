import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getPokemonService, getPokemonDetailService } from "../apis/pokemonApi";

const initialState = {
    pokemon: [],
    status: "idle", // idle, loading, succeeded, failed
    error: null,
    ownedPokemons: []
};

export const homeScreenSlice = createSlice({
    name: "pokemon",
    initialState,
    reducers: {
        //Add owned pokemon
        addOwnedPokemon: (state, action) => {
            state.ownedPokemons.push(action.payload);
        },
        //Remove owned pokemon
        removeOwnedPokemon: (state, action) => {
            state.ownedPokemons = state.ownedPokemons.filter((pokemon) => pokemon.name !== action.payload.name);
        },
        //Remove all owned pokemon
        removeAllOwnedPokemon: (state) => {
            state.ownedPokemons = [];
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchPokemon.pending, (state) => {
                state.status = "loading";
            })
            .addCase(fetchPokemon.fulfilled, (state, action) => {

                state.status = "succeeded";
                state.pokemon = action.payload.results;
            })
            .addCase(fetchPokemon.rejected, (state, action) => {
                state.status = "failed";
                state.error = action.error.message;
            })
    }
});

export const fetchPokemon = createAsyncThunk(
    'pokemon/fetchPokemon',
    async () => {
        try {
            const data = await getPokemonService();
            return data;
        } catch (error) {
            console.log('error');
            console.log(error);
        }
    }
)

export const getPokemonDetail = createAsyncThunk(
    'pokemon/fetchPokemon',
    async (pokemonName) => {
        try {
            const data = await getPokemonDetailService(pokemonName);
            // console.log(data);
            return data;
        } catch (error) {
            console.log('error');
            console.log(error);
        }
    }
)

//export state
export const selectCount = (state) => state.counter.count;
export const selectPokemon = (state) => state.counter.pokemon;
export const selectStatus = (state) => state.counter.status;
export const selectError = (state) => state.counter.error;

export const { increment, decrement, incrementByAmount, reset } = homeScreenSlice.actions;

export default homeScreenSlice.reducer;