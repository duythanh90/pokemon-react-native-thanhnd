import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { PokemonFlatList } from './components/PokemonFlatList'
import { useSelector, useDispatch } from 'react-redux'
import { fetchPokemon } from './homeScreenSlice';

export const HomeScreen = (props) => {

    const pokemon = useSelector(state => state.pokemonStore.pokemon)

    const dispatch = useDispatch();

    useState(() => {
        dispatch(fetchPokemon())
    }, [])

    return (
        <SafeAreaView >
            <View>
                <View style={styles.header} >
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>Pokémon</Text>
                    </View>
                    <View style={styles.bagContainer}>
                        <TouchableOpacity onPress={() => {
                            console.log('Bag pressed')
                            props.navigation.navigate('MyBag')
                            
                        }}>
                            <Image
                                style={styles.bag}
                                resizeMode={'contain'}
                                source={require('../assets/bag.png')}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
               
                <PokemonFlatList navigation={props.navigation} pokemon={pokemon} />
                <View style={styles.imgPokeApiContainer}>
                    <Image
                        style={styles.imgPokeApi}
                        resizeMode={'contain'}
                        source={require('../assets/pokeapi.png')}
                    />
                </View>
            </View>

        </SafeAreaView >
    )

    //Create style
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
    },
    titleContainer: {
        flex: 1,
        alignItems: 'flex-start',
    },
    title: {
        fontSize: 32,
        textAlign: 'left',
        marginLeft: 16,
        marginTop: 24,
        fontFamily: 'HelveticaNeue',
    },
    bagContainer: {
        flex: 1,
        alignItems: 'flex-end',
    },
    bag: {
        marginTop: 15,
        marginRight: 16,

        width: 49,
        height: 52
    },
    imgPokeApiContainer: {
        position: 'absolute',
        width: '100%',
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgPokeApi: {
        width: 110,
        height: 44,
    }
});
