import React from 'react'
import { View, StyleSheet, FlatList } from 'react-native'
import { PokemonFlatListItem } from './PokemonFlatListItem'
export const PokemonFlatList = (props) => {
    return (
        <View style={{
            height: '90%',
            marginTop: 10,
        }}>
            {props.pokemon &&
                <FlatList
                    data={props.pokemon}
                    renderItem={({ item }) => <PokemonFlatListItem navigation={props.navigation} item={item} />}
                />
            }

        </View>
    )
}

const styles = StyleSheet.create({

});
