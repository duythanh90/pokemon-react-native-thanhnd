//Create function return color of pokemon type
function PokemonTypeColors(type) {
    switch (type) {
        case 'normal':
            return {
                primary: '#A8A878',
                secondary: '#C6C6A7',
            };
        case 'fire':
            return {
                primary: '#F08030',
                secondary: '#F5AC78',
            };
        case 'water':
            return {
                primary: '#6890F0',
                secondary: '#9DB7F5',
            };
        case 'electric':
            return {
                primary: '#F8D030',
                secondary: '#FAE078',
            };
        case 'grass':
            return {
                primary: '#78C850',
                secondary: '#A7DB8D',
            };
        case 'ice':
            return {
                primary: '#98D8D8',
                secondary: '#BCE6E6',
            };
        case 'fighting':
            return {
                primary: '#C03028',
                secondary: '#D67873',
            };
        case 'poison':
            return {
                primary: '#A040A0',
                secondary: '#C183C1',
            };
        case 'ground':
            return {
                primary: '#E0C068',
                secondary: '#EBD69D',
            };
        case 'flying':
            return {
                primary: '#A890F0',
                secondary: '#C6B7F5',
            };
        case 'psychic':
            return {
                primary: '#F85888',
                secondary: '#FA92B2',
            };
        case 'bug':
            return {
                primary: '#A8B820',
                secondary: '#C6D16E',
            };
        case 'rock':
            return {
                primary: '#B8A038',
                secondary: '#D1C17D',
            };
        case 'ghost':
            return {
                primary: '#705898',
                secondary: '#A292BC',
            };
        case 'dragon':
            return {
                primary: '#7038F8',
                secondary: '#A27DFA',
            };
        case 'dark':
            return {
                primary: '#705848',
                secondary: '#A29288',
            };
        case 'steel':
            return {
                primary: '#B8B8D0',
                secondary: '#D1D1E0',
            };
        case 'fairy':
            return {
                primary: '#EE99AC',
                secondary: '#F4BDC9',
            };
        default:
            return {
                primary: '#68A090',
                secondary: '#9DB7F5',
            };
    }
}
export { PokemonTypeColors };