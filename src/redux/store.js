import { configureStore } from '@reduxjs/toolkit';
import homeScreenReducer from '../home_screen/homeScreenSlice';
import pokemonDetailReducer from '../pokemon_detail/pokemonDetailSlice';
import pokemonOwnedReducer from '../my_bag/pokemonOwnedSlice';

export default configureStore({
    reducer: {
        pokemonStore: homeScreenReducer,
        pokemonDetailStore: pokemonDetailReducer,
        pokemonOwnedStore: pokemonOwnedReducer,
    },
});