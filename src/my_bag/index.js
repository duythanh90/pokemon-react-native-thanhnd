import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { PokemonFlatList } from './components/PokemonFlatList'
import { useSelector, useDispatch } from 'react-redux'

export const MyBag = (props) => {

    const pokemonsOwned = useSelector(state => state.pokemonOwnedStore.pokemonsOwned)

    const dispatch = useDispatch();

    useState(() => {

    }, [])
    console.log('RENDER MY BAG');
    console.log(pokemonsOwned);
    return (
        <SafeAreaView >
            <View>
                <View style={styles.header} >
                    <View style={styles.backContainer}>
                        <TouchableOpacity style={styles.back} onPress={() => {
                            console.log('Back pressed')
                            props.navigation.goBack()
                        }}>
                            <Image
                                style={styles.backImage}
                                resizeMode={'contain'}
                                source={require('../assets/my_bag_back_icon.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>My Bag</Text>
                    </View>
                </View>
                <PokemonFlatList navigation={props.navigation} pokemon={pokemonsOwned} />

            </View>

        </SafeAreaView >
    )

    //Create style
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
    },
    titleContainer: {
        flex: 1,
        alignItems: 'flex-start',
    },
    title: {
        fontSize: 32,
        textAlign: 'left',
        marginLeft: 16,
        marginTop: 24,
        fontFamily: 'HelveticaNeue',
    },
    bagContainer: {
        flex: 1,
        alignItems: 'flex-end',
    },
    bag: {
        marginTop: 15,
        marginRight: 16,

        width: 49,
        height: 52
    },
    imgPokeApiContainer: {
        position: 'absolute',
        width: '100%',
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgPokeApi: {
        width: 110,
        height: 44,
    },
    backContainer: {
    },
    back: {
        width: 49,
        height: 52,
        marginTop: 15,
        marginLeft: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 15,
        height: 18,
    },

});
