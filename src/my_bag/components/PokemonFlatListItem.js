import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { PokemonTypeColors } from '../../utils/PokemonTypeColors'
import { releasePokemon } from '../pokemonOwnedSlice';

export const PokemonFlatListItem = (props) => {
    let pokemonId = props.item.id
    let pokemonImageUri = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/' + pokemonId + '.png';
    let backgroundColor = PokemonTypeColors('steel').primary;

    const dispatch = useDispatch();

    return (
        <TouchableOpacity style={styles.container}
            onPress={() => {
                props.navigation.navigate('PokemonDetail', {
                    pokemonId: pokemonId,
                    pokemonName: props.item.name,
                    pokemonImageUri: pokemonImageUri,
                })
            }}
        >
            <View
                style={{
                    height: 124,
                    marginHorizontal: 16,
                    borderRadius: 12,
                    backgroundColor: backgroundColor
                }}
            >

            </View>
            <View style={styles.imageContainer}>
                <Image
                    style={styles.img}
                    resizeMode={'center'}
                    source={{ uri: pokemonImageUri }}
                />
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.text}>{props.item.myPokemonName}</Text>
                <TouchableOpacity
                    onPress={() => {
                        console.log('Release pressed')
                        dispatch(releasePokemon(pokemonId))
                    }}
                    style={styles.ownedContainer}>
                    <Text style={styles.txtOwned}>{"Release"}</Text>
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 210,
        justifyContent: 'center'
    },
    imageContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
    },
    textContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
    },
    text: {
        fontSize: 25,
        textAlign: 'left',
        marginLeft: 170,
        marginTop: 50,
        fontFamily: 'HelveticaNeue',
        color: 'white'
    },
    img: {
        width: 161,
        height: 170,
    },
    ownedContainer: {
        backgroundColor: 'black',
        opacity: 0.7,
        borderRadius: 12,
        height: 20,
        width: 120,
        marginTop: 50,
        marginLeft: 170,
        alignItems: 'center',
        paddingHorizontal: 4,
        justifyContent: 'center'
    },
    txtOwned: {

        fontSize: 16,
        color: '#FFFFFF',

    }
});
