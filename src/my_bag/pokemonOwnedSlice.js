import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    pokemonsOwned: [],
};

export const pokemonOwnedSlice = createSlice({
    name: "pokemon_onwed",
    initialState,
    reducers: {
        addPokemon: (state, action) => {
            console.log('ADD POKEMON');
            console.log(action.payload);
            state.pokemonsOwned.push(action.payload);
        },
        releasePokemon: (state, action) => {
            state.pokemonsOwned = state.pokemonsOwned.filter((pokemon) => pokemon.id !== action.payload);
        }
    },
});

export const selectPokemonsOwned = (state) => state.pokemonOwnedSlice.pokemonsOwned;
const { actions, reducer } = pokemonOwnedSlice;
export const { addPokemon, releasePokemon } = actions;

export default reducer;