//Reaact hoock
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';

export const AbilityComponent = (props) => {

    const [abilities, setAbilities] = useState(props.abilities)

    //Map props to state
    useEffect(() => {
        setAbilities(props.abilities)
    }, [props.abilities]);

    return (
        <View>

            {abilities.map((ability, index) => {
                return <View
                    key={index}
                    style={{
                        marginHorizontal: 16,
                        marginTop: 16,
                        backgroundColor: '#E4E3E3',
                        borderRadius: 12,
                        padding: 8,
                    }}>
                    <View>
                        <Text style={styles.txtTitle}>{ability.name}</Text>
                    </View>

                </View>
            })}

        </View>
    )
}

const styles = StyleSheet.create({
    txtTitle: {
        color: '#919191',
        fontWeight: 'bold',
        fontSize: 12,
        fontFamily: 'HelveticaNeue',
    },
    txtContent: {
        color: '#919191',
        fontSize: 12,
        fontFamily: 'HelveticaNeue',
        marginTop: 8
    },
});

