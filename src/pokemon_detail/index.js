import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, TextInput } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { getPokemonDetail } from './pokemonDetailSlice';
import { Dimensions } from 'react-native';
import { AbilityComponent } from './components/AbilityComponent';
import { addPokemon } from '../my_bag/pokemonOwnedSlice';
import { PokemonTypeColors } from '../utils/PokemonTypeColors'
export const PokemonDetail = (props) => {

    const [hideImage, setHideImage] = useState(false);
    const [popupVisible, setPopupVisible] = useState(false);
    const [popupType, setPopupType] = useState('error');
    const [popupTitle, setPopupTitle] = useState('');
    const [popupContent, setPopupContent] = useState('');
    const [popupPokemonName, setPopupPokemonName] = useState('');

    const pokemonDetail = useSelector(state => state.pokemonDetailStore.pokemonDetail)
    const pokemonsOwned = useSelector(state => state.pokemonOwnedStore.pokemonsOwned)

    const dispatch = useDispatch();

    useState(() => {
        dispatch(getPokemonDetail(props.route.params.pokemonName))
    }, [])

    let owned = false;
    if (pokemonsOwned != null) {
        let pokemonOwnedIndex = pokemonsOwned.findIndex(pokemon => pokemon.name == props.route.params.pokemonName);
        if (pokemonOwnedIndex != -1) {
            owned = true;
        } else {
            owned = false;
        }
    }

    let pokemonName = props.route.params.pokemonName;
    let pokemonImageUri = props.route.params.pokemonImageUri;

    let screenWidth = Dimensions.get('window').width;
    let screenHeight = Dimensions.get('window').height;
    let moves = [];
    let type = '';
    let firstType = '';
    let height = '';
    let weight = '';
    let abilities = [];
    let pokemonTypeBackgroundColor = '';

    if (pokemonDetail != null) {
        if (pokemonDetail.moves != null && pokemonDetail.moves.length > 0) {
            pokemonDetail.moves.forEach((move, index) => {
                moves.push({
                    index: index,
                    name: move.move.name
                });
            });
        }
        if (pokemonDetail.types != null && pokemonDetail.types.length > 0) {
            pokemonDetail.types.forEach((pokemonDetailType, index) => {
                if (index == 0)
                    firstType = pokemonDetailType.type.name;

                if (index == pokemonDetail.types.length - 1)
                    type += pokemonDetailType.type.name
                else
                    type += pokemonDetailType.type.name + ', '
            });
        } else {
            type = 'Unknown';
        }
        if (pokemonDetail.abilities != null && pokemonDetail.abilities.length > 0) {
            pokemonDetail.abilities.forEach((pokemonDetailAbility, index) => {
                abilities.push({
                    index: index,
                    name: pokemonDetailAbility.ability.name,
                    url: pokemonDetailAbility.ability.url,
                });
            });
        }
        if (pokemonDetail.height != null) {
            height = pokemonDetail.height;
        } else {
            height = 'Unknown';
        }
        if (pokemonDetail.weight != null) {
            weight = pokemonDetail.weight;
        }
        else {
            weight = 'Unknown';
        }
    }

    pokemonTypeBackgroundColor = PokemonTypeColors(firstType).primary;

    const catchPokemon = () => {
        //50 - 50 chance to catch pokemon
        let random = Math.floor(Math.random() * 2);
        if (random == 0) {
            setPopupTitle('Oops');
            setPopupContent('Failed to catch pokemon');
            setPopupVisible(true);
            setPopupType('error');
        }
        else {
            setPopupTitle('Congratulation');
            setPopupVisible(true);
            setPopupType('success');
        }
    }

    const onPopupButtonClicked = () => {
        setPopupVisible(false);
        if (popupType == 'success') {
            dispatch(addPokemon({
                myPokemonName: popupPokemonName,
                ...pokemonDetail
            }));
        }
    }


    return (
        <View style={{ flex: 1, backgroundColor: 'transparent' }}>

            <View style={[styles.topContainer, {
                backgroundColor: pokemonTypeBackgroundColor
            }]}>
                <View style={styles.pokeballContainer}>

                    <Image
                        style={styles.pokeball_bg}
                        resizeMode={'contain'}
                        source={require('../assets/pokeball_bg.png')}
                    />
                </View>

                <View style={styles.backContainer}>
                    <TouchableOpacity onPress={() => {
                        props.navigation.goBack();
                    }}>
                        <Image
                            style={styles.backimage}
                            resizeMode={'contain'}
                            source={require('../assets/back.png')}
                        />
                    </TouchableOpacity>
                    <View

                        style={[styles.pokemonNameContainer, {
                            width: screenWidth
                        }]}
                    >
                        <Text style={styles.txtPokemonName}>{pokemonName}</Text>
                        {owned ?
                            <View style={styles.ownedContainer}>
                                <Text style={styles.txtOwned}>{"Owned"}</Text>
                            </View>
                            :
                            <TouchableOpacity onPress={() => {
                                catchPokemon();
                            }} style={styles.ownedContainer}>
                                <Text style={styles.txtOwned}>{"Catch"}</Text>
                            </TouchableOpacity>
                        }


                    </View>
                </View>
            </View>
            <ScrollView
                scrollEventThrottle={16}

                onScroll={(event) => {
                    // Hide the image
                    if (event.nativeEvent.contentOffset.y > 1) {
                        setHideImage(true);
                    } else {
                        setHideImage(false);
                    }
                }}
                bounces={false} style={{
                    marginTop: -50,
                    borderTopLeftRadius: 30,
                    borderTopRightRadius: 30,

                }}>
                <View style={styles.bottomContainer}>
                    <View style={{
                        marginLeft: 16,
                        marginTop: 46,
                    }}>
                        <Text style={styles.txtBold}>About</Text>
                    </View>
                    <View
                        style={styles.horizontalContainer}
                    >
                        <View style={{
                            width: 150
                        }}>
                            <Text style={styles.txtTitle}>Type</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={styles.txtBold}>{type}</Text>
                        </View>
                    </View>
                    <View
                        style={styles.horizontalContainer}
                    >
                        <View style={{
                            width: 150
                        }}>
                            <Text style={styles.txtTitle}>Height</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={styles.txtBold}>{height}</Text>
                        </View>
                    </View>
                    <View
                        style={styles.horizontalContainer}
                    >
                        <View style={{
                            width: 150
                        }}>
                            <Text style={styles.txtTitle}>Weight</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={styles.txtBold}>{weight}</Text>
                        </View>
                    </View>
                    <View style={{
                        marginLeft: 16,
                        marginTop: 46,
                    }}>
                        <Text style={styles.txtBold}>Abilities</Text>
                    </View>
                    {/* Loop throught abilities */}
                    <AbilityComponent abilities={abilities} />
                    <View style={{
                        marginLeft: 16,
                        marginTop: 46,
                    }}>
                        <Text style={styles.txtBold}>Moves</Text>
                    </View>
                    <View style={{ paddingTop: 16, paddingBottom: 16 }}>
                        <View style={styles.movesContainer}>
                            {
                                moves.map((move) =>
                                    <View key={move.index} style={styles.moves}>
                                        <Text style={styles.txtMoves}>{move.name}</Text>
                                    </View>
                                )}
                        </View>
                    </View>
                </View>

            </ScrollView>

            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
            }}>
                {!hideImage &&
                    <Image
                        style={{
                            width: 185,
                            height: 185,
                            position: 'absolute',
                            top: 100,
                            left: screenWidth / 2 - 100,
                        }}
                        resizeMode={'contain'}
                        source={{ uri: pokemonImageUri }}
                    />
                }

            </View>
            {
                popupVisible
                &&
                <View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: screenWidth,
                    height: screenHeight,
                    backgroundColor: 'rgba(0,0,0,0.5)',

                }}>
                    {/* Rounded view width = 80%, border radius = 12, background color = white */}
                    <View style={{
                        width: screenWidth * 0.8,
                        height: 130,
                        backgroundColor: 'white',
                        borderRadius: 24,
                        position: 'absolute',
                        top: screenHeight / 3,
                        left: screenWidth / 2 - (screenWidth * 0.8) / 2,
                        alignItems: 'center',
                        paddingTop: 12,
                        paddingBottom: 12,
                    }}>
                        <Text
                            style={{
                                fontSize: 12,
                                fontWeight: 'bold',
                                color: 'black',
                            }}
                        > {popupTitle} </Text>

                        {popupType == 'success' ?
                            <TextInput style={{
                                width: screenWidth * 0.8 - 24,
                                height: 32,
                                backgroundColor: '#F2F2F2',
                                borderRadius: 16,
                                marginTop: 12,
                                paddingLeft: 12,
                                paddingRight: 12,
                            }}
                                placeholder={'Enter pokemon name'}
                                onChangeText={(text) => {
                                    setPopupPokemonName(text);
                                }}
                                value={popupPokemonName}
                            />

                            :
                            <Text
                                style={{
                                    fontSize: 12,
                                    marginTop: 12,
                                    color: 'black',
                                }}
                            > {popupContent} </Text>

                        }
                        {/* Rounded button */}
                        <TouchableOpacity
                            onPress={() => {
                                onPopupButtonClicked();
                            }}
                            style={{
                                width: 80,
                                height: 32,
                                backgroundColor: '#FF3A3A',
                                borderRadius: 16,
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: 12,
                            }}
                        >
                            <Text style={{
                                color: 'white',
                                fontSize: 12,
                            }}>{
                                    popupType == 'success' ? 'Save' : 'Close'
                                }</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            }

        </View>


    )

}

const styles = StyleSheet.create({
    topContainer: {
        width: '100%',
        height: 250,
        backgroundColor: '#FFA329'
    },
    pokemonNameContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 16,
        paddingHorizontal: 16,

    },
    txtPokemonName: {
        color: 'white',
        fontSize: 30,
    },
    pokeballContainer: {
        position: 'absolute',
        bottom: -40,
        right: 0,
        overflow: 'hidden',
    },
    backContainer: {
        position: 'absolute',
        top: 0,
    },
    bottomContainer: {
        marginTop: 0,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
    pokeball_bg: {
        width: 173,
        height: 173,
    },
    backimage: {
        width: 15,
        height: 18,
        marginLeft: 16,
        marginTop: 60,
    },
    ownedContainer: {
        backgroundColor: 'black',
        opacity: 0.4,
        borderRadius: 12,
        height: 20,
        width: 70,
        alignItems: 'center',
        justifyContent: 'center'

    },
    txtOwned: {
        fontFamily: 'HelveticaNeue',
        fontSize: 16,
        color: '#FFFFFF',

    },
    txtBold: {
        fontSize: 12,
        fontWeight: 'bold',

        fontFamily: 'HelveticaNeue',
    },
    txtTitle: {
        color: '#919191',
        fontWeight: 'bold',
        fontSize: 12,
        fontFamily: 'HelveticaNeue',
    },
    txtContent: {
        color: '#919191',
        fontSize: 12,
        fontFamily: 'HelveticaNeue',
        marginTop: 8
    },
    horizontalContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginLeft: 16,
        marginTop: 16,
        alignItems: 'center',
    },
    movesContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginHorizontal: 16,
        marginTop: 16,
    },
    moves: {
        backgroundColor: '#E4E3E3',
        borderRadius: 99,
        paddingTop: 4,
        paddingBottom: 4,
        paddingLeft: 10,
        paddingRight: 10,
        marginRight: 8,
        marginBottom: 8,
    },
    txtMoves: {
        color: '#919191',
        fontSize: 12,
        fontFamily: 'HelveticaNeue',
    },


});
