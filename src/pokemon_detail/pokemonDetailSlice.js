import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {  getPokemonDetailService } from "../apis/pokemonApi";

const initialState = {
    pokemonDetail: {},
    status: "idle", // idle, loading, succeeded, failed
    error: null
};

export const pokemonDetailSlice = createSlice({
    name: "pokemon_detail",
    initialState,
    reducers: {
        
    },
    extraReducers: (builder) => {
        builder
            .addCase(getPokemonDetail.pending, (state) => {
                state.status = "loading";
            })
            .addCase(getPokemonDetail.fulfilled, (state, action) => {
                state.status = "succeeded";
                state.pokemonDetail = action.payload;
            })
            .addCase(getPokemonDetail.rejected, (state, action) => {
                state.status = "failed";
                state.error = action.error.message;
            })
    }
});


export const getPokemonDetail = createAsyncThunk(
    'pokemon/fetchPokemonDetail',
    async (pokemonName) => {
        try {
            const data = await getPokemonDetailService(pokemonName);
            console.log(data);
            return data;
        } catch (error) {
            console.log('error');
            console.log(error);
        }
    }
)

export const selectPokemonDetail = (state) => state.pokemonDetailSlice.pokemonDetail;
export const selectStatus = (state) => state.pokemonDetailSlice.status;
export const selectError = (state) => state.pokemonDetailSlice.error;

export default pokemonDetailSlice.reducer;