import { requestApi } from "../axios";

export function getPokemonService() {
    return requestApi(`/pokemon?limit=100000&offset=0`, 'GET');
}
export function getPokemonDetailService(pokemonName) {
    return requestApi(`/pokemon/` + pokemonName, 'GET');
}
