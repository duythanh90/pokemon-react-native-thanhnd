import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'https://pokeapi.co/api/v2/',
    timeout: 60000,
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': "*"
    },
});


// export function setTokenAxios(token) {
//     axiosInstance.defaults.headers.common.Authorization = token;
// }
// method: 'GET' | 'POST' | 'PUT' | 'DELETE'
export function requestApi(url, method, data, headers) {
    return new Promise(async (resolve, reject) => {
        try {
            console.log('REQUEST');
            console.log('url: ' + url);
            const response = await axiosInstance.request({
                data,
                headers: {
                    ...headers
                },
                method,
                url
            });
            resolve(response.data);
        } catch (e) {
        }
    });
}
